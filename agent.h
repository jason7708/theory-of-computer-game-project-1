#pragma once
#include <string>
#include <random>
#include <sstream>
#include <map>
#include <type_traits>
#include <algorithm>
#include <vector>
#include "board.h"
#include "action.h"

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
};

class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};

/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
/*
class rndenv : public random_agent {
public:
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args),
		space({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }), popup(0, 9) {}

	virtual action take_action(const board& after) {
		std::shuffle(space.begin(), space.end(), engine);
		for (int pos : space) {
			if (after(pos) != 0) continue;
			board::cell tile = popup(engine) ? 1 : 2;
			return action::place(pos, tile);
		}
		return action();
	}

private:
	std::array<int, 16> space;
	std::uniform_int_distribution<int> popup;
};
*/
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
class rndenv : public random_agent {
public:
	int slide_direction;
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args){
		tile_bag.clear();
		slide_direction=5;
	}
	void get_user_direction(const int user_move) {
		slide_direction = user_move;
	}
	void clear_bag(void) {
		tile_bag.clear();
	}
	void initial(void) {
		slide_direction=5;
	}
	virtual action take_action(const board& after){
		
		if(tile_bag.empty()){
			tile_bag.push_back (1);
			tile_bag.push_back (2);
			tile_bag.push_back (3);
		}

		std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
		switch (slide_direction) {
			case 5:
				initial_space={0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
				std::shuffle(initial_space.begin(), initial_space.end(),engine);
				for(int pos : initial_space){
					if(after(pos) != 0) continue;
					board::cell tile = tile_bag.back();
					tile_bag.pop_back();
					return action::place(pos, tile);
				}
				break;
			case 0: 
				space={12, 13, 14, 15};   //up
				break;
			case 1: 
				space={0, 4, 8, 12};   //right
				break;
			case 2: 
				space={0, 1, 2, 3};   //down
				break;
			case 3: 
				space={3, 7, 11, 15};   //left
				break;
			default:
				break;
		}
		std::shuffle(space.begin(), space.end(), engine);
		//std::cout << "evil_dir:"<<slide_direction<<'\n';
		for(int pos : space){
			if (after(pos) != 0) continue;
			board::cell tile = tile_bag.back();
			tile_bag.pop_back();
			//std::cout << "pos:"<<pos<<'\n';
			return action::place(pos, tile);
		}
		return action();
	}
private:
	std::vector<int> tile_bag;
	std::array<int, 4> space;
	std::array<int, 16> initial_space;
	
};
// RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
/**
 * dummy player
 * select a legal action randomly
 */
class player : public random_agent {
public:
	player(const std::string& args = "") : random_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }),/*RRRR*/ player_direction(0)/*RRRR*/ {}

	int player_choose(void){
		return player_direction;
	}	
	virtual action take_action(const board& before) {
		std::shuffle(opcode.begin(), opcode.end(), engine);
		for (int op : opcode) {
			board::reward reward = board(before).slide(op);

			if (reward != -1){
				/*RR*/
				player_direction = op;
				/*RR*/
				return action::slide(op);
			} 
		}
		return action();
	}

private:
	std::array<int, 4> opcode;
	int player_direction; //RRRRRRRRRRRRRR
};
